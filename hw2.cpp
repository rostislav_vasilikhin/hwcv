#include <cstdio>
#include <iostream>
#include <fstream>
#include <string>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

void onMouse(int event, int x, int y, int flags, void* userdata)
{
    Mat buf = *(Mat*)userdata;
    if(event == EVENT_LBUTTONDOWN)
    {

    }
}

void usage()
{
    cout << "Usage: hw2 <video>" << endl;
}

int main(int argc ,char* argv[])
{
    string videoFname;
    if(argc < 2)
    {
        usage(); return -1;
    }
    else
    {
        videoFname = argv[1];
    }

    if(videoFname.size() == 1 && videoFname[0] >= '0' && videoFname[0] < '9')
    {
        VideoCapture vc(videoFname[0] - '0');

        Mat img, buffer;
        setMouseCallback("video", onMouse, (void*)&buffer);
        do
        {
            vc >> img;
            if(!img.empty())
            {
                //processImg(img);
                waitKey(1);

            }
        }
        while(!img.empty());
    }
    else
    {
        VideoCapture vc(videoFname);

        Mat img;
        do
        {
            vc >> img;
            if(!img.empty())
            {
                //processImg(img);
                waitKey(1);
            }
        }
        while(!img.empty());

    }

    return 0;
}
