#include <cstdio>
#include <iostream>
#include <fstream>
#include <string>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;


void usage()
{
    cout << "Usage: hw1 <image>" << endl;
}

void processImg(Mat img, float threshold1 = 80.0, float threshold2 = 160.0, float k = 1.0)
{
    Mat gray;
    cvtColor(img, gray, CV_BGR2GRAY);

    Mat cannied, gradx, grady;
//    Sobel(gray, gradx, CV_8U, 1, 0, 3, 1, 127);
//    Sobel(gray, grady, CV_8U, 0, 1, 3, 1, 127);
    Canny(gray, cannied, threshold1, threshold2);

    cannied = ~cannied;

    Mat distMap;
    distanceTransform(cannied, distMap, CV_DIST_L2, CV_DIST_MASK_PRECISE);

    //cout << distMap << endl;

    //some processing
    vector<Mat> chans, intg(3); split(img, chans);
    for(int i = 0; i < 3; i++)
    {
        integral(chans[i], intg[i], CV_32F);
    }
    Mat result(img.size(), CV_8UC3);
    Rect imRect(Point(0, 0), Size(img.cols-1, img.rows-1));
    for(int y = 0; y < result.rows; y++)
    {
        for(int x = 0; x < result.cols; x++)
        {
            int w = int(distMap.at<float>(y, x)*k)/2;
            Rect r(x - w, y - w, w*2 + 1, w*2 + 1);
            r &= imRect; Vec3d v;
            for(int i = 0; i < 3; i++)
            {
                v[i] = intg[i].at<float>(r.br()) - intg[i].at<float>(r.y, r.x+r.width) -
                       intg[i].at<float>(r.y+r.height, r.x) + intg[i].at<float>(r.tl());
                v[i] = std::max(0.0, std::min(255.0, v[i]*1.0/r.area()) );
            }
            result.at<Vec3b>(y, x) = Vec3b(v);
        }
    }

    imshow("orig", img);
    imshow("result", result);

    imshow("canny", cannied);
}

int main(int argc ,char* argv[])
{
    string imgFname;
    if(argc < 2)
    {
        usage(); return -1;
    }
    else
    {
        imgFname = argv[1];
    }

    int param1, param2, k = 100;
    namedWindow("orig");
    createTrackbar("param1", "orig", &param1, 255);
    createTrackbar("param2", "orig", &param2, 255);
    createTrackbar("k", "orig", &k, 255);

    if(imgFname.size() == 1)
    {
        VideoCapture vc(0);

        Mat img;
        do
        {
            vc >> img;
            if(!img.empty())
            {
                processImg(img, param1, param2, k/255.0);
                waitKey(1);
            }
        }
        while(!img.empty());
    }
    else
    {
        Mat img = imread(imgFname);
        if(img.empty())
        {
            cout << "Failed to load image " << imgFname << endl;
            return -1;
        }

        while(waitKey(10) != 'q')
        {
            processImg(img, param1, param2, k/255.0);
        }

    }

    return 0;
}
